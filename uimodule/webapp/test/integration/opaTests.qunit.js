/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"autoServicio/xs_autoServicio/test/integration/AllJourneys"
	], function () {
		QUnit.start();
	});
});