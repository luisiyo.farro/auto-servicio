sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageBox"
], function (Controller, MessageBox) {
	"use strict";
	var Z_OD_SCP_BASRRC001_SRV;
	var Z_OD_SCP_CORE_0001_SRV;
	var thes;
	var centro = "4100";
	var nuevo;
	return Controller.extend("autoServicio.xs_autoServicio.controller.Home", {
		onInit: function () {
			thes = this;
			Z_OD_SCP_BASRRC001_SRV = this.getOwnerComponent().getModel("Z_OD_SCP_BASRRC001_SRV");
			Z_OD_SCP_CORE_0001_SRV = this.getOwnerComponent().getModel("Z_OD_SCP_CORE_0001_SRV");
			this.modelEnabled();
			thes.getServicioMarca();
			thes.getServicioMovilidad(centro);
		},

		onIFrame: function (oEvent) {
			var spaceURL = window.location.origin;
			spaceURL = spaceURL.substring(0, 38);
			var deploySrc = spaceURL + 'usercard.cfapps.us10.hana.ondemand.com';
			var oSource = oEvent.getSource();
			var htmlPage = new sap.ui.core.HTML({
				preferDOM: true,
				content: "<iframe src='" + deploySrc + "' frameborder='0' height='500px' width='100%'></iframe>"
			});
			var Popover = new sap.m.Popover({ title: "Estructura Organizacional", verticalScrolling: false, placement: sap.m.PlacementType.Bottom, contentWidth: "500px", contentHeight: "500px", content: htmlPage });
			Popover.openBy(oSource);
		},

		modelEnabled: function () {
			var json = {
				placa: true,
				btnSiguiente: false,
				fomularioContacto: false,
				formularioServicios: false
			};
			var jsonModel = new sap.ui.model.json.JSONModel(json);
			this.getView().setModel(jsonModel, "enabled");
		},
		onPressSiguiente: function (oEvent) {
			var error = false;
			var oEmail = thes.byId("iptEmail");
			var oNombres = thes.byId("iptNombres");
			var oApellidos = thes.byId("iptApellidos");
			var oCelular = thes.byId("iptCelular");
			var oPlaca = thes.byId("iptPlaca");
			var oMarca = thes.byId("cbMarca");
			var oModelo = thes.byId("cbModelo");
			var oYear = thes.byId("iptYear");
			error = thes.validarCampoVacio(oEmail, "ipt", error);
			error = thes.validarCampoVacio(oNombres, "ipt", error);
			//error = thes.validarCampoVacio(oApellidos, "ipt", error);
			error = thes.validarCampoVacio(oCelular, "ipt", error);
			error = thes.validarCampoVacio(oPlaca, "ipt", error);
			error = thes.validarCampoVacio(oMarca, "ipt", error);
			error = thes.validarCampoVacio(oModelo, "ipt", error);
			error = thes.validarCampoVacio(oYear, "ipt", error);
			if (error) {
				return;
			}

			var modelEnabled = this.getView().getModel("enabled");
			var dataEnabled = modelEnabled.getData();
			dataEnabled.btnSiguiente = false;
			dataEnabled.placa = false;
			dataEnabled.fomularioContacto = false;
			dataEnabled.formularioServicios = true;
			modelEnabled.refresh();
			this.newModelServicios();

			thes.Cod_Marca = oMarca.getSelectedKey();
			thes.modelo = oModelo.getSelectedKey();

			thes.getServicioMantenimientos(thes.Cod_Marca, thes.modelo);
			thes.getServicioPromociones(thes.Cod_Marca, thes.modelo);
			thes.getServicioAdicionales(thes.Cod_Marca, thes.modelo);
			thes.getServicioPrepagados(thes.Idvehi);

		},

		validarCampoVacio: function (objeto, tipo, error) {
			var valor;
			switch (tipo) {
				case "ipt":
					valor = objeto.getValue();
					break;
				case "cb":
					valor = objeto.getSelectedKey();
					break;
				case "dtp":
					valor = objeto.getDateValue();
			}

			if (valor === "") {
				objeto.setValueState(sap.ui.core.ValueState.Error);
				objeto.setValueStateText("Ingrese un valor Válido");
				error = true;
			} else {
				objeto.setValueState(sap.ui.core.ValueState.Success);
				objeto.setValueStateText("");
			}

			return error;
		},

		getDatosVigilancia: function () {

			var oPlaca = thes.byId("iptPlaca");
			var placa = (oPlaca.getValue()).toUpperCase();

			//var validateEmail = thes.validateValue(email);

			if (placa === "") {
				oPlaca.setValueState(sap.ui.core.ValueState.Error);
				oPlaca.setValueStateText("Ingrese un valor Válido");
				return;
			}

			/*if (validateEmail !== "") {
				thes.limpiarCamposMenosEmail();
				oEmail.setValueState(sap.ui.core.ValueState.Error);
				oEmail.setValueStateText(validateEmail);
				return;
			}*/

			thes.limpiarCamposMenosPlaca();

			oPlaca.setValueState(sap.ui.core.ValueState.Success);
			oPlaca.setValueStateText("");

			var arrayFilters = [];

			var filter = new sap.ui.model.Filter("LicenseNum", sap.ui.model.FilterOperator.EQ, placa);
			arrayFilters.push(filter);
			var filter = new sap.ui.model.Filter("Procesado", sap.ui.model.FilterOperator.EQ, false);
			arrayFilters.push(filter);

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_SRV.read("/CitaVigilanteSet", {
				filters: arrayFilters,
				success: function (result) {
					sap.ui.core.BusyIndicator.hide();
					if (result.results.length === 0) {

						MessageBox.error("Registrarse en vigilancia");

						return;
					}

					thes.modelEnabled();

					var modelEnabled = thes.getView().getModel("enabled");
					var dataEnabled = modelEnabled.getData();
					dataEnabled.btnSiguiente = true;
					modelEnabled.refresh();

					//thes.getDatosContacto(result.results[0].EMail);
					var jsonModel = new sap.ui.model.json.JSONModel(result.results[0]);
					thes.getView().setModel(jsonModel, "datosVigilancia");
					thes.getDatosVehiculo(result.results[0].LicenseNum);

				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}

			});

		},

		getDatosContacto: function (Partner) {

			var arrayFilters = [];

			var filter = new sap.ui.model.Filter("Partner", sap.ui.model.FilterOperator.EQ, Partner);

			arrayFilters.push(filter);
			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_SRV.read("/InterlocutorSet", {
				filters: arrayFilters,
				success: function (result) {
					sap.ui.core.BusyIndicator.hide();
					if (result.results.length === 0) {
						var oEnabled = thes.getView().getModel("enabled");
						var enabled = oEnabled.getData();
						enabled.fomularioContacto = true;
						oEnabled.refresh();
						return;
					}
					var jsonModel = new sap.ui.model.json.JSONModel(result.results[0]);
					thes.getView().setModel(jsonModel, "datosContacto");

				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}

			});

		},

		getDatosVehiculo: function (placa) {

			if (placa === "") {
				return;
			}

			var arrayFilters = [];

			var filter = new sap.ui.model.Filter("Matricula", sap.ui.model.FilterOperator.EQ, placa);

			arrayFilters.push(filter);

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_SRV.read("/VehiculoSet", {
				filters: arrayFilters,
				success: function (result) {
					sap.ui.core.BusyIndicator.hide();
					if (result.results.length === 0) {
						var oEnabled = thes.getView().getModel("enabled");
						var enabled = oEnabled.getData();
						enabled.fomularioContacto = true;
						oEnabled.refresh();
						nuevo = true;
						return;
					}
					nuevo = false;
					var jsonModel = new sap.ui.model.json.JSONModel(result.results[0]);
					thes.getView().setModel(jsonModel, "datosVehiculo");

					thes.Cod_Marca = result.results[0].Cod_Marca;
					thes.Idvehi = result.results[0].Idvehi;

					thes.getServicioModelo(thes.Cod_Marca);
					thes.getServicioPrepagados(thes.Idvehi);
					thes.getServicioPrepagados(thes.Idvehi);
					thes.getDatosContacto(result.results[0].Codigocliente);

				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}

			});

		},

		getServicioMantenimientos: function (marca, modelo) {
			var arrayFilters = [];
			var filter = new sap.ui.model.Filter("Marca", sap.ui.model.FilterOperator.EQ, marca);
			arrayFilters.push(filter);
			var filter = new sap.ui.model.Filter("Modelo", sap.ui.model.FilterOperator.EQ, modelo);
			arrayFilters.push(filter);
			var filter = new sap.ui.model.Filter("TipoServicio", sap.ui.model.FilterOperator.EQ, "1");
			arrayFilters.push(filter);

			this.getServicios(arrayFilters, "servicioMantenimientos", "1");
		},

		getServicioPromociones: function (marca, modelo) {
			var arrayFilters = [];
			var filter = new sap.ui.model.Filter("Marca", sap.ui.model.FilterOperator.EQ, marca);
			arrayFilters.push(filter);
			var filter = new sap.ui.model.Filter("Modelo", sap.ui.model.FilterOperator.EQ, modelo);
			arrayFilters.push(filter);
			var filter = new sap.ui.model.Filter("TipoServicio", sap.ui.model.FilterOperator.EQ, "2");
			arrayFilters.push(filter);

			this.getServicios(arrayFilters, "servicioPromociones", "2");
		},

		getServicioAdicionales: function (marca, modelo) {
			var arrayFilters = [];
			var filter = new sap.ui.model.Filter("Marca", sap.ui.model.FilterOperator.EQ, marca);
			arrayFilters.push(filter);
			var filter = new sap.ui.model.Filter("Modelo", sap.ui.model.FilterOperator.EQ, modelo);
			arrayFilters.push(filter);
			var filter = new sap.ui.model.Filter("TipoServicio", sap.ui.model.FilterOperator.EQ, "3");
			arrayFilters.push(filter);

			this.getServicios(arrayFilters, "servicioAdicionales", "3");
		},

		getServicioPrepagados: function (Idvehi) {
			var arrayFilters = [];
			var filter = new sap.ui.model.Filter("Idvehi", sap.ui.model.FilterOperator.EQ, Idvehi);
			arrayFilters.push(filter);
			var filter = new sap.ui.model.Filter("TipoServicio", sap.ui.model.FilterOperator.EQ, "4");
			arrayFilters.push(filter);

			this.getServicios(arrayFilters, "servicioPrepagados", "4");
		},

		getServicios: function (arrayFilters, nameModel, tipoServicio) {
			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_SRV.read("/CitaServicioDispoSet", {
				filters: arrayFilters,
				success: function (result) {
					sap.ui.core.BusyIndicator.hide();
					if (result.results.length === 0) {

						if (tipoServicio === "4") {
							thes.byId("cbServPrePaga").setVisible(false);
						}

						return;
					}

					if (tipoServicio === "4") {
						thes.byId("cbServPrePaga").setVisible(true);
						MessageBox.information("La unidad posee un servicio pre-pagado");
					}

					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, nameModel);
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}

			});

		},

		onChangeMarca: function () {

			var marcaKey = thes.byId("cbMarca").getSelectedKey();
			if (marcaKey !== "") {
				thes.getServicioModelo(marcaKey);
			}
		},

		getServicioModelo: function (modeloTecnico) {

			var arrayFilters = [];
			var filter = new sap.ui.model.Filter("Modelotecnico", sap.ui.model.FilterOperator.EQ, modeloTecnico);
			arrayFilters.push(filter);
			var filter = new sap.ui.model.Filter("Modelotecnico", sap.ui.model.FilterOperator.EQ, modeloTecnico);
			arrayFilters.push(filter);

			Z_OD_SCP_BASRRC001_SRV.read("/ModeloSet", {
				filters: arrayFilters,
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "modelo");
				},
				error: function (err) { }

			});

		},

		getServicioMarca: function () {

			var arrayFilters = [];
			var filter = new sap.ui.model.Filter("Charactname", sap.ui.model.FilterOperator.EQ, "ZAP_MARCA");
			arrayFilters.push(filter);

			Z_OD_SCP_CORE_0001_SRV.read("/CaracteriticasSet", {
				filters: arrayFilters,
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, "marca");
				},
				error: function (err) { }
			});
		},

		getServicioMovilidad: function (sociedad) {

			var arrayFilters = [];
			var filter = new sap.ui.model.Filter("Werks", sap.ui.model.FilterOperator.EQ, sociedad);
			arrayFilters.push(filter);
			var filter = new sap.ui.model.Filter("TipoMovilidad", sap.ui.model.FilterOperator.EQ, '01');
			arrayFilters.push(filter);

			this.getTipoMovilidad(arrayFilters, "servicioMovilidad");

		},

		getTipoMovilidad: function (arrayFilters, nameModel) {
			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_SRV.read("/ConfTipoMovSet", {
				filters: arrayFilters,
				success: function (result) {
					sap.ui.core.BusyIndicator.hide();
					if (result.results.length === 0) {
						return;
					}
					var jsonModel = new sap.ui.model.json.JSONModel(result.results);
					thes.getView().setModel(jsonModel, nameModel);
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}

			});

		},

		validateValue: function (oValue) {
			var rexMail = /^\w+[\w-+\.]*\@\w+([-\.]\w+)*\.[a-zA-Z]{2,}$/;
			if (!oValue.match(rexMail)) {
				return ("'" + oValue + "' Correo no Válido");
			}
			return "";
		},

		newModelServicios: function (oEvent) {
			var json = {
				mantenimiento: "",
				servicioExpress: false,
				servicioAdicional: [],
				promociones: [],
				servicioPrepagado: "",
				comentario: "",
				servicioMovilidad: "",
				servicioTaxi: false,
				calle: "",
				codigoPostal: "",
				diaTraslado: new Date(),
				horarioTraslado: ""
			};

			var jsonModel = new sap.ui.model.json.JSONModel(json);
			thes.getView().setModel(jsonModel, "servicios");

		},

		onBtnCancel: function () {
			thes.limpiarCampos();
			thes.modelEnabled();
		},

		limpiarCampos: function () {
			var jsonModel = new sap.ui.model.json.JSONModel();
			thes.getView().setModel(jsonModel, "datosContacto");
			thes.getView().setModel(jsonModel, "datosVehiculo");
			thes.getView().setModel(jsonModel, "servicioAdicionales");
			thes.getView().setModel(jsonModel, "servicioMantenimientos");
			//thes.getView().setModel(jsonModel, "servicioMovilidad");
			thes.getView().setModel(jsonModel, "servicioPromociones");
			thes.getView().setModel(jsonModel, "servicios");
			thes.byId("iptCalleMovilidad").setValue();
			thes.byId("ckbMovilidad").setSelected(false);
		},

		limpiarCamposMenosPlaca: function () {

			var oPlaca = thes.byId("iptPlaca");
			var placa = oPlaca.getValue();

			var jsonModel = new sap.ui.model.json.JSONModel();
			thes.getView().setModel(jsonModel, "datosContacto");
			thes.getView().setModel(jsonModel, "datosVehiculo");
			thes.getView().setModel(jsonModel, "servicioAdicionales");
			thes.getView().setModel(jsonModel, "servicioMantenimientos");
			//thes.getView().setModel(jsonModel, "servicioMovilidad");
			thes.getView().setModel(jsonModel, "servicioPromociones");
			this.getView().setModel(jsonModel, "servicios");

			thes.byId("iptPlaca").setValue(placa);

		},

		onBtnEnviar: function (oEvent) {
			var servicios = this.getView().getModel("servicios").getData();
			var contacto = this.getView().getModel("datosContacto").getData();
			var vehiculo = this.getView().getModel("datosVehiculo").getData();
			var vigilancia = this.getView().getModel("datosVigilancia").getData();
			var tiposMovilidad = this.getView().getModel("servicioMovilidad").getData();

			var fechaActual = new Date();
			var fechaFinCita = moment(fechaActual).subtract(3, "minutes").toDate();

			var arrayServicios = [];

			/*var movilidad = "";
			var precioMovilidad = "";
			if (servicios.servicioMovilidad !== "") {
				var servicioMovilidad = servicios.servicioMovilidad.split("*");
				movilidad = servicioMovilidad[0];
				precioMovilidad = servicioMovilidad[1];
			}*/

			if (servicios.mantenimiento !== "") {
				var mantenimiento = servicios.mantenimiento.split("*");
				var jsonServicio = {
					"Plnal": mantenimiento[0],
					"Plnty": mantenimiento[2],
					"Zaehl": mantenimiento[3],
					"HdId": "1",
					"CodCita": "",
					"TipoServicio": "1",
					"CodServicio": mantenimiento[1]
				};
				arrayServicios.push(jsonServicio);
			}

			for (var x in servicios.servicioAdicional) {
				var adicional = servicios.servicioAdicional[x];
				if (adicional !== "") {
					var servicioAdicional = adicional.split("*");
					var jsonServicio = {
						"Plnal": servicioAdicional[0],
						"Plnty": servicioAdicional[2],
						"Zaehl": servicioAdicional[3],
						"HdId": "1",
						"CodCita": "",
						"TipoServicio": "3",
						"CodServicio": servicioAdicional[1]
					};
					arrayServicios.push(jsonServicio);
				}
			}

			for (var x in servicios.promociones) {
				var promocion = servicios.promociones[x];
				if (promocion !== "") {
					var promociones = promocion.split("*");
					var jsonServicio = {
						"Plnal": promociones[0],
						"Plnty": promociones[2],
						"Zaehl": promociones[3],
						"HdId": "1",
						"CodCita": "",
						"TipoServicio": "2",
						"CodServicio": promociones[1]
					};
					arrayServicios.push(jsonServicio);
				}
			}

			if (servicios.servicioPrepagado !== "") {
				var servicioPrepagado = servicios.servicioPrepagado.split("*");
				var jsonServicio = {
					"Plnal": servicioPrepagado[0],
					"Plnty": servicioPrepagado[2],
					"Zaehl": servicioPrepagado[3],
					"HdId": "1",
					"CodCita": "",
					"TipoServicio": "4",
					"CodServicio": servicioPrepagado[1]
				};
				arrayServicios.push(jsonServicio);
			}

			if (arrayServicios.length === 0) {
				MessageBox.error("Seleccione un servicio");
				return;
			}

			if (servicios.servicioTaxi) {
				var error = false;
				var oCalle = thes.byId("iptCalleTaxi");
				error = thes.validarCampoVacio(oCalle, "ipt", error);
				if (error) {
					return;
				}
			}

			var precioMovilidad = "";
			var tipoMovilidad = "";
			if (thes.byId("ckbMovilidad").getSelected()) {
				var error = false;
				var oCalle = thes.byId("iptCalleMovilidad");
				error = thes.validarCampoVacio(oCalle, "ipt", error);
				if (error) {
					return;
				}
				precioMovilidad = tiposMovilidad[0].ValetCosto;
				tipoMovilidad = tiposMovilidad[0].TipoMovilidad;
			}
			if (precioMovilidad === "") {
				precioMovilidad = "0.00";
			}
			var jsonCita = {
				"OrigenCita": "P",
				"Bukrs": contacto.Bukrs,
				"Werks": centro,
				"HdId": "1",
				"Scpuser": "",
				"Scpapp": "",
				"UpdVehi": "",
				"UpdPartner": "",
				"UpdPagador": "",
				"UpdConductor": "",
				"UpdCita": "X",
				"CodCita": "",
				"TipoCita": "G",
				"Idvehi": vehiculo.Idvehi,
				"FleetVin": "",
				"Marca": "",
				"Anno": "",
				"ColorExt": "",
				"LicenseNum": "",
				"Modelo": "",
				"Kilometraje": "",
				"Partner": contacto.Partner,
				"TipoDeIc": "",
				"Rfc": "",
				"Nombre": "",
				"Apellido": "",
				"EMail": "",
				"Telefono": "",
				"Country": "",
				"Region": "",
				"City1": "",
				"StrSuppl1": "",
				"Street": "",
				"HouseNo": "",
				"PostlCod1": "",
				"Express": servicios.servicioExpress,
				"CodAsesor": "",
				"FechaInicio": moment(fechaActual).format('YYYY-MM-DD[T]HH:mm:ss'),
				"FechaFin": moment(fechaFinCita).format('YYYY-MM-DD[T]HH:mm:ss'),
				"Estatus": "I",
				"EstatusTiempo": "",
				"Observacion": servicios.comentario,
				"FechaInicioR": "",
				"FechaFinR": "",
				"MovDirec": servicios.calle,
				"MovCodp": "",
				"NovTaxi": servicios.servicioTaxi,
				"MovFecha": moment(fechaActual).format('YYYY-MM-DD[T]HH:mm:ss'),
				"MovTipo": tipoMovilidad,
				"MovCosto": precioMovilidad.toString(),
				"Pagador": contacto.Partner,
				"TipoDeIcPa": "",
				"RfcPa": "",
				"Name1Pa": "",
				"Name2Pa": "",
				"EmailPa": "",
				"TelefonoPa": "",
				"CountryPag": "",
				"RegionPag": "",
				"CityPag": "",
				"Colonia1Pa": "",
				"Calle1Pa": "",
				"NoEdifPa": "",
				"CodpostalPag": "",
				"Conductor": contacto.Partner,
				"TipoPersonaCon": "",
				"RfcCon": "",
				"NombreCon": "",
				"ApellidoCon": "",
				"EmailCon": "",
				"TelefonoCon": "",
				"PaisCon": "",
				"EstadoCon": "",
				"PoblacionCon": "",
				"ColoniaCon": "",
				"CalleCon": "",
				"NumeroCon": "",
				"PostalCon": "",
				"UuidVigil": vigilancia.Uuid,
				"Cservicios": arrayServicios,
				"Cireset": []
			};

			if (nuevo) {

				jsonCita.Idvehi = "";
				jsonCita.Marca = vehiculo.Cod_Marca;
				jsonCita.Anno = vehiculo.Modelo;
				jsonCita.LicenseNum = vehiculo.Matricula;
				jsonCita.Modelo = vehiculo.Cod_Modelo;
				jsonCita.FleetVin = vehiculo.Nrochasis;

				jsonCita.Partner = "";
				jsonCita.Pagador = "";
				jsonCita.Conductor = "";
				jsonCita.Nombre = contacto.Name1;
				jsonCita.Apellido = contacto.Name2;
				jsonCita.EMail = contacto.EMail;
				jsonCita.Telefono = contacto.Telephone;
			}

			thes.crearCita(jsonCita);

		},

		autoServProcesado: function (id) {

			var url = "/CitaVigilanteSet(" + id + ")";

			Z_OD_SCP_BASRRC001_SRV.read(url, {
				success: function (result) {
					sap.ui.core.BusyIndicator.hide();

				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}

			});

		},

		actualizarVigilante: function () {

			var vigilancia = thes.getView().getModel("datosVigilancia").getData();
			var contacto = thes.getView().getModel("datosContacto").getData();
			var vehiculo = thes.getView().getModel("datosVehiculo").getData();

			var json = {
				Uuid: vigilancia.Uuid,
				EMail: contacto.EMail,
				Nombre: contacto.Name1,
				Apellido: contacto.Name2,
				Telefono: contacto.Telephone,
				LicenseNum: vehiculo.Matricula,
				Marca: vehiculo.Cod_Marca,
				Modelo: vehiculo.Cod_Modelo,
				YearModelo: vehiculo.Modelo,
				Procesado: true
			};

			var url = "/CitaVigilanteSet('" + vigilancia.Uuid + "')";

			sap.ui.core.BusyIndicator.show();
			Z_OD_SCP_BASRRC001_SRV.update(url, json, {
				success: function (result) {
					sap.ui.core.BusyIndicator.hide();
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
				}

			});

		},

		onSelServTaxi: function (oEvent) {
			if (oEvent.getParameters().selected === true) {
				thes.byId("iptCalleTaxi").setVisible(true);
			} else {
				thes.byId("iptCalleTaxi").setVisible(false);
			}
		},

		onSelServMovi: function (oEvent) {
			if (oEvent.getParameters().selected === true) {
				thes.byId("iptCalleMovilidad").setVisible(true);
			} else {
				thes.byId("iptCalleMovilidad").setVisible(false);
			}
		},

		onSelServExpress: function (oEvent) {

			if (oEvent.getParameters().selected === true) {
				thes.byId("cbServAdici").setVisible(false);
				thes.byId("cbPromo").setVisible(false);
			} else {
				thes.byId("cbServAdici").setVisible(true);
				thes.byId("cbPromo").setVisible(true);
			}

		},

		crearCita: function (jsonCita) {

			Z_OD_SCP_BASRRC001_SRV.create("/Cita_OperacionesSet", jsonCita, {
				success: function (result) {
					thes.mostrarMensajes(result.Cireset.results);

					if (result.Cireset.results[0].Type !== "Error") {
						if (nuevo) {
							thes.actualizarVigilante();
						} else {
							thes.autoServProcesado(thes.Idvehi);
						}
					}

					thes.limpiarCampos();
					thes.modelEnabled();

				},
				error: function (err) {

				}
			});

		},

		mostrarMensajes: function (mensajes) {

			/*var mensajes = [{
				"Type": "E",
				"Message": "Mensaje de prueba"
			}];*/

			for (var i = 0; i < mensajes.length; i++) {
				switch (mensajes[i].Type) {
					case "E":
						mensajes[i].Type = "Error";
						break;
					case "S":
						mensajes[i].Type = "Success";
						break;
					case "W":
						mensajes[i].Type = "Warning";
						break;
					case "I":
						mensajes[i].Type = "Information";
						break;
				}
			}

			var oMessageTemplate = new sap.m.MessageItem({
				type: '{Type}',
				title: '{Message}'
			});

			var oModel = new sap.ui.model.json.JSONModel(mensajes);

			var oBackButton = new sap.m.Button({
				icon: sap.ui.core.IconPool.getIconURI("nav-back"),
				visible: false,
				press: function () {
					thes.oMessageView.navigateBack();
					this.setVisible(false);
				}
			});

			thes.oMessageView = new sap.m.MessageView({
				showDetailsPageHeader: false,
				itemSelect: function () {
					oBackButton.setVisible(true);
				},
				items: {
					path: "/",
					template: oMessageTemplate
				}
			});

			thes.oMessageView.setModel(oModel);

			thes.oDialog = new sap.m.Dialog({
				resizable: true,
				content: thes.oMessageView,
				beginButton: new sap.m.Button({
					press: function () {
						thes.oDialog.close();
					},
					text: "Close"
				}),
				customHeader: new sap.m.Bar({
					contentMiddle: [
						new sap.m.Text({
							text: "Mensajes"
						})
					],
					contentLeft: [oBackButton]
				}),
				contentHeight: "50%",
				contentWidth: "50%",
				verticalScrolling: false
			});

			thes.oMessageView.navigateBack();
			thes.oDialog.open();

		}

	});
});